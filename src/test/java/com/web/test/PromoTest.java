package com.web.test;

import io.restassured.path.json.JsonPath;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class PromoTest {

    private static final ThreadLocal<WebDriver> driverThread = new ThreadLocal<>();

    private static final String USERNAME = System.getenv("BROWSERSTACK_USERNAME");
    private static final String ACCESS_KEY = System.getenv("BROWSERSTACK_ACCESS_KEY");
    private static final String URL = "http://" + USERNAME + ":" + ACCESS_KEY + "@hub-cloud.browserstack.com/wd/hub";
    public Actions a;
    public WebDriverWait w;

    @BeforeTest(alwaysRun = true)
    @Parameters({"config", "environment"})
    public void setup(String configFile, String environment) throws MalformedURLException {
        JsonPath jsonPath = JsonPath.from(new File("src/test/resources/web/config/" + configFile));
        Map<String, String> capDetails = new HashMap<>();
        capDetails.putAll(jsonPath.getMap("capabilities"));
        capDetails.putAll(jsonPath.getMap("environments." + environment));
        DesiredCapabilities caps = new DesiredCapabilities(capDetails);
        driverThread.set(new RemoteWebDriver(new URL(URL), caps));
    }

    @Test
    public void testSearchBrowserStack() throws InterruptedException {
        WebDriver driver = driverThread.get();
        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.promo.com/templates");
        Thread.sleep(5000);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        List<WebElement> options = driver.findElements(By.xpath("//li[@class='menu-list-item']"));
        for (int i = 1; i < options.size(); i++) {
            WebElement ele = driver.findElement(By.xpath("(//DIV[@class='menu-list-item-name']/a)[" + i + "]"));
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView({behavior: 'smooth', block: 'center' });", ele);
            ele.click();

            Thread.sleep(2000);
            js.executeScript("window.scrollBy(0,1000)");
            WebElement val = driver.findElement(By.xpath("//span[@class='breadcrumbs__item__name']"));
            Assert.assertTrue(val.isDisplayed());
        }
         }


    public void Explicitwait1(WebDriver driver, WebElement element) {

        w = new WebDriverWait(driver, 50);
        w.until(ExpectedConditions.visibilityOf(element));
    }

    @AfterTest(alwaysRun = true)
    public void teardown() {
        JavascriptExecutor js = (JavascriptExecutor) driverThread.get();
        js.executeScript("browserstack_executor: {\"action\": \"setSessionStatus\", \"arguments\": {\"status\": \"passed\", \"reason\": \"BrowserStack search passed\"}}");
        driverThread.get().quit();
        driverThread.remove();
    }

}
